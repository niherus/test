import serial, tkinter, time

def COM_init(Port = 'COM1', Baudrate = 38400, ByteSize = 8, Parity = 'N',
             StopBit = 1, Timeout = 0, XonXoff = 0, RtsCts = 0, DsrDtr = 0 ):
    s = serial.Serial()
    s.baudrate = Baudrate
    s.port = Port
    s.bytesize = ByteSize
    s.parity = Parity
    s.stopbits = StopBit
    s.timeout = Timeout
    s.xonxoff = XonXoff
    s.rtscts = RtsCts
    s.dsrdtr = DsrDtr
    s.open()
    return s

def COM_scan():
    COM_List = []
    for i in range(256):
        try:
            s = serial.Serial('COM'+str(i))
            COM_List.append(s.portstr)
            s.close()
        except serial.SerialException:
            pass
    return COM_List

def Get_Date(stFlow, QuantityOfBytes = 2, Signed = True,FirstDateBit = 0,
             LastDateBit = 7, BytesOrder = True, BitsOrder = 'big'):
    stFlow.write(b'1')
    TempDate = 0
    TempByte = 0
    i = 1
    Delta = LastDateBit - FirstDateBit + 1
    while i <= QuantityOfBytes:
        if BytesOrder:
            while stFlow.inWaiting() == 0:
                pass
            TempByte = int.from_bytes(stFlow.read(), byteorder=BitsOrder, signed=False)
            TempDate |= ((TempByte >> FirstDateBit) & ((0b1 << Delta ) - 1)) << ((i - 1) * Delta)
            i += 1
        else:
            while stFlow.inWaiting() == 0:
                pass
            TempByte = int.from_bytes(stFlow.read(), byteorder=BitsOrder, signed=False)
            TempDate |= ((TempByte >> FirstDateBit) & ((0b1 << Delta) - 1)) << ((QuantityOfBytes - i) * Delta)
            i += 1
    if Signed:
        if TempDate >> (QuantityOfBytes *Delta - 1) == 1:
            Sample = -(1+((TempDate ^ (0b1 << (QuantityOfBytes * Delta - 1)))
                          ^ ((0b1 << (QuantityOfBytes * Delta - 1) ) - 1)))
        else:
            Sample = TempDate
    return Sample

def Quit():
    Flag[1] = True
    exit()
    print(str(Flag[1]))

def WritingData(Data, Dir):
    file = open(Dir, 'a')
    file.writelines(Data)
    file.close()

def FileCreation(event, name):

    if Flag[2] ^ True:
        RecordButton['text'] = ' () '
        i = 1
        while True:
            FileName[0] = name + '_' + str(i) + '.txt'
            try:
                file = open(FileName[0], 'r')
            except FileNotFoundError:
                Flag[2] = True
                RecordButton['text'] = ' [_] '
                break
            file.close()
            i += 1
    else:
        Flag[2] = False
        RecordButton['text'] = ' () '


def Flag_Inverter(event, Flag):
    if Flag[0]:
        Flag[0] = False
        PlayButton['text'] = ' |> '
    else:
        Flag[0] = True
        PlayButton['text'] = ' ||'

class App():

    def white(self):
        self.lines=[]
        self.y_lastpos = 0
        self.x_lastpos = 0
        self.plot_Width = int(self.W_Width)
        self.plot_Height = int(self.W_Height*0.8)
        self.c.create_rectangle(self.X_Shift, self.Y_Shift, self.X_Shift+self.plot_Width,
                                self.Y_Shift+self.plot_Height, fill="white")
        self.lineRedraw=self.c.create_line(0, 0, 0, self.plot_Width, fill="red")
        for x in range(int(self.Max_Time_Scale * self.sps / 1000)):
            self.lines.append(self.c.create_line(x, 0, x, 0, fill="blue"))
        
       
    def grid(self):
        self.grid_HorizontalLines()
        self.grid_VerticalLines()

    def grid_HorizontalLines(self):
        if self.Min_Amp_Scale <= 0:
            x1 = self.X_Shift
            y11 = self.Y_Shift
            y12 = (self.plot_Height*self.Max_Amp_Scale/(self.Max_Amp_Scale-self.Min_Amp_Scale))/self.Max_Amp_Scale
            x2 = self.plot_Width+self.X_Shift
            y21 = self.Y_Shift
            y22 = (self.plot_Height*self.Max_Amp_Scale/(self.Max_Amp_Scale-self.Min_Amp_Scale))/self.Max_Amp_Scale

            for y in range(self.Max_Amp_Scale, 0, -self.Amp_grid):
                self.c.create_line(x1, y11 + int(y*y12), x2, y21 + int(y*y22), fill="#333333",dash=(4, 4))
                self.c.create_text(10, int(y*y22)+7, fill="#999999", text=str(int((self.Max_Amp_Scale-y))),
                                   anchor="w")

            x1 = self.X_Shift
            y11 = self.Y_Shift + self.plot_Height*self.Max_Amp_Scale/(self.Max_Amp_Scale-self.Min_Amp_Scale)
            y12 = self.plot_Height /(self.Max_Amp_Scale-self.Min_Amp_Scale)
            y13 = self.Min_Amp_Scale
            x2 = self.plot_Width + self.X_Shift
            y21 = self.Y_Shift + self.plot_Height*self.Max_Amp_Scale/(self.Max_Amp_Scale-self.Min_Amp_Scale)
            y22 = self.plot_Height/(self.Max_Amp_Scale-self.Min_Amp_Scale)
            y23 = self.Min_Amp_Scale

            for y in range(self.Min_Amp_Scale, 0, self.Amp_grid):
                self.c.create_line(x1, int(y11 + y12*(y - y13)), x2, int(y21 + y22*( y - y23)),
                                   fill="#333333", dash=(4, 4))
                self.c.create_text(10, int(y21 -self.Y_Shift + y22*(y - y23)) + 7,
                               fill="#999999", text=str(int((self.Min_Amp_Scale - y  ))), anchor="w")
        else:
            x1 = self.X_Shift
            y11 = self.Y_Shift
            y12 = (self.plot_Height * self.Max_Amp_Scale / (self.Max_Amp_Scale
                                                            - self.Min_Amp_Scale)) / self.Max_Amp_Scale
            x2 = self.plot_Width + self.X_Shift
            y21 = self.Y_Shift
            y22 = (self.plot_Height * self.Max_Amp_Scale / (self.Max_Amp_Scale
                                                            - self.Min_Amp_Scale)) / self.Max_Amp_Scale

            for y in range(self.Max_Amp_Scale, 0, -self.Amp_grid):
                if self.Max_Amp_Scale - y >= self.Min_Amp_Scale:
                    self.c.create_line(x1, y11 + int(y * y12), x2, y21 + int(y * y22), fill="#333333", dash=(4, 4))
                    self.c.create_text(10, int(y * y22) + 7, fill="#999999", text=str(int((self.Max_Amp_Scale - y))),
                                   anchor="w")
    def grid_VerticalLines(self):
        x1 = self.plot_Width/self.Max_Time_Scale
        y1 = self.Y_Shift
        x2 = self.plot_Width/self.Max_Time_Scale
        y2 = self.plot_Height+self.Y_Shift

        for x in range(0,self.Max_Time_Scale,self.Time_grid):
            self.c.create_line(x*x1, y1, x*x2, y2, fill="#333333",dash=(4, 4))
            self.c.create_text(x*x2+3, y2+10, fill="black", text=str(x/1000)+" s", anchor="w")

    def addPoint(self,val):
        
        self.data[self.xpos]=val
        
        if (val < self.Max_Amp_Scale) & (val > self.Min_Amp_Scale):
            x1 = self.x_lastpos 
            y1 = (int(self.plot_Height*self.Max_Amp_Scale/(self.Max_Amp_Scale-self.Min_Amp_Scale)) -
                  self.y_lastpos+self.Y_Shift)
            x2 = int((self.xpos*self.plot_Width*1000/(self.sps*self.Max_Time_Scale))+self.X_Shift)
            y2 = (int(self.plot_Height*self.Max_Amp_Scale/(self.Max_Amp_Scale-self.Min_Amp_Scale))+self.Y_Shift -
                  int(val*self.plot_Height/(self.Max_Amp_Scale-self.Min_Amp_Scale)))
            self.y_lastpos = int(val * self.plot_Height / (self.Max_Amp_Scale-self.Min_Amp_Scale))
            self.x_lastpos = x2
        elif val >= self.Max_Amp_Scale:
            x1 = self.x_lastpos 
            y1 = (int(self.plot_Height*self.Max_Amp_Scale/(self.Max_Amp_Scale-self.Min_Amp_Scale)) -
                  self.y_lastpos+self.Y_Shift)
            x2 = (self.xpos*self.plot_Width*1000/(self.sps*self.Max_Time_Scale))+self.X_Shift
            y2 = self.Y_Shift
            self.y_lastpos = (self.Max_Amp_Scale * self.plot_Height / (self.Max_Amp_Scale-self.Min_Amp_Scale))
            self.x_lastpos = x2
        elif val <= self.Min_Amp_Scale:

            x1 = self.x_lastpos 
            y1 = (int(self.plot_Height*self.Min_Amp_Scale/(self.Max_Amp_Scale-self.Min_Amp_Scale)) -
                  self.y_lastpos+self.Y_Shift + self.plot_Height)
            x2 = (self.xpos*self.plot_Width*1000/(self.sps*self.Max_Time_Scale))+self.X_Shift
            y2 = self.Y_Shift + self.plot_Height
            self.y_lastpos = int(self.Min_Amp_Scale * self.plot_Height / (self.Max_Amp_Scale-self.Min_Amp_Scale))
            self.x_lastpos = x2

        if self.xpos > 0:
            self.c.coords(self.lines[self.xpos], (x1, y1, x2, y2))

        if self.xpos < int(self.Max_Time_Scale * self.sps / 1000):
           self.c.coords(self.lineRedraw, (x2 + 1, self.Y_Shift, x2 + 1, self.plot_Height + self.Y_Shift))
        self.xpos += 1
       

 
        if self.xpos > int(self.Max_Time_Scale*self.sps/1000) - 1:
            self.xpos = 0
        t.update()

    def __init__(self, t, W_Width = 800, W_Height = 512):
        self.W_Width = W_Width
        self.W_Height = W_Height
        self.i = 0
        self.t = 0
        self.X_Shift = 5
        self.Y_Shift = 15
        self.Max_Amp_Scale = 1024
        self.Min_Amp_Scale = -1024
        self.Max_Time_Scale = 4096
        self.Time_grid = 100
        self.Amp_grid = 200
        self.xpos = 0
        self.timeStart = time.perf_counter()
        self.c = tkinter.Canvas(t, width=W_Width, height=W_Height)
        self.fps = 100
        self.sps = 200
        self.data = [0] * int(self.Max_Time_Scale * self.sps / 1000)
        self.c.pack()
        self.white()
        self.grid()
t = tkinter.Tk()
Flag = [True, False, False]
FileName = ['']
#*********** TOOLBAR ***********************#

toolbar = tkinter.Frame(t)

PlayButton = tkinter.Button(toolbar, text = ' |> ')
PlayButton.bind('<Button-1>',lambda event: Flag_Inverter(event, Flag))
PlayButton.pack(side = tkinter.LEFT, padx = 2, pady = 2)

RecordButton = tkinter.Button(toolbar, text = ' () ')
RecordButton.bind('<Button-1>',lambda event: FileCreation(event, 'New'))
RecordButton.pack(side = tkinter.LEFT, padx = 2, pady = 2)

toolbar.pack(side = tkinter.TOP, fill = tkinter.X)
#*********** MAIN MENU ***********************#
menu = tkinter.Menu(t)
t.config(menu=menu)
FileMenu = tkinter.Menu(menu)
menu.add_cascade(label = 'File', menu = FileMenu)
FileMenu.add_command(label = 'File name')
FileMenu.add_command(label = 'Directory')
FileMenu.add_separator()
FileMenu.add_command(label = 'Exit', command = Quit)

UARTMenu = tkinter.Menu(menu)
menu.add_cascade(label = 'COM port setting', menu = UARTMenu)
UARTMenu.add_command(label = 'Update list of Ports')
UARTMenu.add_command(label = 'Port')
UARTMenu.add_command(label = 'Speed')
UARTMenu.add_command(label = 'Quantity of Bits')
UARTMenu.add_command(label = 'Parity')
UARTMenu.add_command(label = 'Stopbits')

DataMenu = tkinter.Menu(menu)
menu.add_cascade(label = 'Date setting', menu = DataMenu)
DataMenu.add_command(label = 'Quantity of channels')
DataMenu.add_command(label = 'Quantity of bytes')
DataMenu.add_command(label = 'Date design')
DataMenu.add_command(label = 'Marks setting')
DataMenu.add_command(label = 'Channel design')

a = App(t, 1000, 800)

In_Ser_PORT = COM_scan()

if In_Ser_PORT:
    stFlow = COM_init(In_Ser_PORT[0])
    n = 0
    start = time.perf_counter()
    while True:
        if Flag[0]:
            
            if (time.perf_counter() - start) >= 1./a.sps:
                Date1 = Get_Date(stFlow)
                a.addPoint(Date1)
                if Flag[2]:
                    WritingData(str((time.perf_counter())*1000) + '\n', FileName[0])
                start = time.perf_counter()
            
        else:
            t.update()
            start = time.perf_counter()
        if Flag[1]:
            break
#Fuck the system

